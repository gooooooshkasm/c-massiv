﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    const int N = 7;
    int massiv[N][N];
    int i, j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            massiv[i][j] = i + j;
            cout << massiv[i][j]<<"   ";
        }
        cout << '\n'<<'\n';
    }
   
    int summa = 0;
    time_t t;
    time(&t);
    int day = localtime(&t)->tm_mday;
    for ( int x = 0; x < N; x++)
    {
        summa += massiv[day % N][x];
    }
    cout << summa << endl;

    return 0;
}